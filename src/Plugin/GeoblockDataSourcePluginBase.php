<?php

declare(strict_types=1);

namespace Drupal\geoblock\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * The base implementation for a geoblock data source plugin.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
abstract class GeoblockDataSourcePluginBase extends PluginBase implements GeoblockDataSourcePluginInterface {

}
