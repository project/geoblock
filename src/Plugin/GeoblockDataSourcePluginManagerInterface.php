<?php

declare(strict_types=1);

namespace Drupal\geoblock\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * An interface used to describe the geoblock data source plugin manager.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
interface GeoblockDataSourcePluginManagerInterface extends PluginManagerInterface {

}
